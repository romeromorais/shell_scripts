# shell_scripts

repositório de instaladores

```shell
#!/bin/bash

echo """
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+  ____                   _      ___           _        _ _                               +
+ / ___| _ __   __ _ _ __| | __ |_ _|_ __  ___| |_ __ _| | |                              +
+ \___ \| '_ \ / _' | '__| |/ /  | || '_ \/ __| __/ _' | | |                              +
+  ___) | |_) | (_| | |  |   <   | || | | \__ \ || (_| | | |                              +
+ |____/| .__/ \__,_|_|  |_|\_\ |___|_| |_|___/\__\__,_|_|_|                              +
+       |_|                                                                               +
+ instalador autonomo do apache-spark 3.2.0                                               +
+ desenvolvido por Romerito Morais https://www.linkedin.com/in/romeritomorais/            +
+ testados nas distribuiçõs derivados de RHEL, Debian                                     +
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""

export USER_DIST=$(cat /etc/os-release | head -3 | tail -1 | cut -d "=" -f 2)
export USER_HOME=$HOME

SPARK="spark-3.2.0"
HADOOP_VERSION="bin-hadoop3.2"
JUPYTER_LAB_VERSION="3.1.1"
INSTALL_DIRECTORY=$(pwd)
INSTALL_BIN_SPARK="https://archive.apache.org/dist/spark/${SPARK}/${SPARK}-${HADOOP_VERSION}.tgz"

sudo rm -rf /opt/apache-spark-${SPARK:6:6}
sudo rm -rf /usr/share/applications/jupyterlab.desktop
sudo rm -rf /usr/local/spark-${SPARK:6:6}
sudo mkdir -p /opt/apache-spark-${SPARK:6:6}/src
```
